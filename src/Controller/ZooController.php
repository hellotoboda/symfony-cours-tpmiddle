<?php

namespace App\Controller;

use App\Entity\Zoo;
use App\Form\ZooType;
use App\Repository\ZooRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/zoo")
 */
class ZooController extends AbstractController
{
    /**
     * @Route("/", name="app_zoo_index", methods={"GET"})
     */
    public function index(ZooRepository $zooRepository): Response
    {
        return $this->render('zoo/index.html.twig', [
            'zoos' => $zooRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="app_zoo_new", methods={"GET", "POST"})
     */
    public function new(Request $request, ZooRepository $zooRepository): Response
    {
        $zoo = new Zoo();
        $form = $this->createForm(ZooType::class, $zoo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $zooRepository->add($zoo);
            return $this->redirectToRoute('app_zoo_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('zoo/new.html.twig', [
            'zoo' => $zoo,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_zoo_show", methods={"GET"})
     */
    public function show(Zoo $zoo): Response
    {
        return $this->render('zoo/show.html.twig', [
            'zoo' => $zoo,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="app_zoo_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, Zoo $zoo, ZooRepository $zooRepository): Response
    {
        $form = $this->createForm(ZooType::class, $zoo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $zooRepository->add($zoo);
            return $this->redirectToRoute('app_zoo_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('zoo/edit.html.twig', [
            'zoo' => $zoo,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="app_zoo_delete", methods={"POST"})
     */
    public function delete(Request $request, Zoo $zoo, ZooRepository $zooRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$zoo->getId(), $request->request->get('_token'))) {
            $zooRepository->remove($zoo);
        }

        return $this->redirectToRoute('app_zoo_index', [], Response::HTTP_SEE_OTHER);
    }
}
