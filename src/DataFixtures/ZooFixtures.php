<?php

namespace App\DataFixtures;

use App\Entity\Zoo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ZooFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $zoo = new Zoo();
        $zoo->setNom("CCI Lyon");
        $zoo->setDescription("ZOO de lyon 9");
        $manager->persist($zoo);
        $manager->flush();
    }
}
